import React, {Component} from 'react';
import axios from "axios";
import "./Characters.css"

class Characters extends Component {

    state = {
        isLoading: true,
        characters: [],
    }

    componentDidMount() {
        setTimeout(() => {
        const {charactersLinks} = this.props;
        const requests = charactersLinks.map(char => axios(char))
        // console.log(`requests`,requests)
        Promise.all(requests)
            .then(res => {
                // console.log(`res`, res)
                const names = res.map(resp => resp.data.name)
                this.setState({characters: names, isLoading: false})
            })

        }, 2500)

    }




    render() {

        const {characters, isLoading} = this.state;
        const charactersList = characters.map((char,index) => {
            return <div className="character">
                <div>{index}</div>
                <div className="name">{char}</div>
            </div>
        })

        return (
            <div>
                {isLoading && <div>Data is loading... Please wait</div>}
                {!isLoading && charactersList}
            </div>
        );
    }
}

export default Characters;