import React, {Component} from 'react';
import "./Film.css"
import Characters from "../Characters/Characters";

class Film extends Component {

    state = {
        filmIsOpen: false,
    }


    showMore = () => {
        this.setState({filmIsOpen: true})
    }

    showMoreOff = () => {
        this.setState({filmIsOpen: false})
    }

    render() {

        const {film} = this.props;
        const {filmIsOpen} = this.state;

        console.log(film)

        return (
            <li className="oneFilm">
                <div>
                    {film.name}
                    {!filmIsOpen && <button onClick={this.showMore}> Детальнее...</button>}
                    {filmIsOpen && <button onClick={this.showMoreOff}> Скрыть...</button>}
                </div>
                <div>
                    {filmIsOpen && <div>
                        {`"Эпизод": ${film.episodeId}`}
                    </div>}
                </div>
                <div>
                    {filmIsOpen && <div>
                        {`"Титры": ${film.openingCrawl}`}
                    </div>}
                </div>
                <div>
                    {filmIsOpen &&
                    <Characters charactersLinks={film.characters}/>}
                </div>
            </li>
        );
    }
}

export default Film;

