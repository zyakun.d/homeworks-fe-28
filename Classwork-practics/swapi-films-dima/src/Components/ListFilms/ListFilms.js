import React, {Component} from 'react';
import Film from "../Film/Film";
import "./ListFilms.css"

class ListFilms extends Component {

    render() {

        const {films} = this.props

        const listFilms = films.map(elem => {return <Film key={elem.id} film={elem}/>})

        return (
            <ol className="listOfFilms">
                {listFilms}
            </ol>
        );
    }
}

export default ListFilms;