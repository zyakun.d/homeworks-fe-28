import React, {Component} from "react";
import './App.css';
import axios from "axios";
import ListFilms from "./Components/ListFilms/ListFilms";


class App extends Component{

  state = {
    isLoading: true,
    films: []
  }

  componentDidMount() {
      setTimeout(() => {
          axios('https://ajax.test-danit.com/api/swapi/films')
              .then(res => {
                  this.setState({films: res.data, isLoading: false})
              })
      }, 2500)
  }

  render() {
        const {isLoading} = this.state
    return (
        <div className="App">
            {!isLoading && <ListFilms films={this.state.films}/>}
            {isLoading && <div>Data is loading... Please wait</div>}
        </div>
    );
  }

}

export default App;
