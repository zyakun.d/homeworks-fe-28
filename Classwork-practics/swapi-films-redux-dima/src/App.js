import React, {useState} from "react";
import './App.css';
import AppRouters from "./Components/Routers/AppRouters";


const App = () => {

    const [user, setUser] = useState(localStorage.getItem("authorized user") ? JSON.parse(localStorage.getItem("authorized user")) : null)

    const LogOut = () => {

        localStorage.setItem("authorized user", null)
        setUser(null)
    }




    return (
        <AppRouters user={user}
                    setUser={setUser}
                    LogOut={LogOut}
        />
    );
}

export default App;
