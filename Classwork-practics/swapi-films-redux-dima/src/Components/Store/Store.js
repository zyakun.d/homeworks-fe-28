import {createStore} from "redux";

const initialState = {
    films: {
        data: [],
        isLoading: true,
        error: null
    },

    film: {
        data: null,
        isLoading: true,
        error: null
    },

    characters: {
        data: [],
        isLoading: true,
        error: null
    }
}

const reducer = (state = initialState, action) => {

    switch (action.type) {
        case 'SET_FILMS': {
            return {...state, films: {...state.films, data: action.payload, isLoading: false} }
        }

        case 'SET_FILM': {
            return {...state, film: {...state.film, data: action.payload, isLoading: false} }
        }

        case 'SET_CHARACTERS': {
            return {...state, characters: {...state.characters, data: action.payload, isLoading: false} }
        }

        default:
            return state;
    }

}

const store = createStore(
    reducer,
    window.__REDUX_DEVTOOLS_EXTENSION__ && window.__REDUX_DEVTOOLS_EXTENSION__()
)

export default store;