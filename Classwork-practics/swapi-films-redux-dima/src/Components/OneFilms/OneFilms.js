import React, {useEffect} from 'react';
import "./OneFilms.css"
import axios from "axios";
import {useHistory, useParams} from "react-router-dom";
import Loader from "../Loader/Loader";
import Characters from "../Characters/Characters";
import {connect, useDispatch, useSelector} from "react-redux";

const OneFilms = () => {

    const film = useSelector(state => state.film.data)
    const isLoading = useSelector(state => state.film.isLoading)
    const dispatch = useDispatch()

    const params = useParams()
    const history = useHistory()

    useEffect(()=> {
            axios(`https://ajax.test-danit.com/api/swapi/films/${params.filmID}`)
                .then(res => {
                    dispatch({type: 'SET_FILM', payload: res.data})
                })
    },[params.filmID])

    return (
        <div className="oneFilm">
            {isLoading && <Loader/>}
            {!isLoading &&  <div className="Film">
                    <div className="filmTitle">
                        <h4>{film ? film.name : ''}</h4>
                        {<button onClick={()=>history.push("/films")}>На главную</button>}
                    </div>
                    <h3>
                        {`"Эпизод": ${film ? film.episodeId : ''}`}
                    </h3>
                    <h4>
                        {`"Титры": ${film ? film.openingCrawl : ''}`}
                    </h4>
                    <h5>
                        <Characters charactersLinks={film ? film.characters : []}/>
                    </h5>
                </div>
            }
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        film: state.film.data,
        isLoading: state.film.isLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setFilms: (film) => dispatch({type: 'SET_FILM', payload: film})
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(OneFilms);
