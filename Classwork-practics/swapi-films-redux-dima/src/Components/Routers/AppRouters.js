import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import OneFilms from "../OneFilms/OneFilms";
import Error404 from "../Error404/Error404";
import Films from "../Films/Films";
import Login from "../Login/Login";
import ProtectedRoute from "./ProtectedRoute";
import Header from "../Header/Header";

const AppRouters = ({setUser, user, LogOut}) => {

    const isLoggedIn = !!user;

    return (
        <div>
            <Header isLoggedIn={isLoggedIn}
                    login={user ? user.login : null}
                    LogOut={LogOut}

            />
            <Switch>
                <Redirect exact from="/" to="/films"/>
                {isLoggedIn && <Redirect exact from="/login" to="/"/>}
                <ProtectedRoute exact path={"/films"} isLoggedIn={isLoggedIn}>
                    <Films/>
                </ProtectedRoute>
                <ProtectedRoute exact path={"/films/:filmID"} isLoggedIn={isLoggedIn}>
                    <OneFilms/>
                </ProtectedRoute>
                <Route exact path={"/login"}>
                    <Login setUser={setUser}/>
                </Route>
                <Route exact path={"*"}>
                    <Error404/>
                </Route>
            </Switch>
        </div>

    );
};

export default AppRouters;