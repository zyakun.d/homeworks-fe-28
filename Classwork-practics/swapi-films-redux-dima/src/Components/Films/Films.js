import React, {useEffect} from 'react';
import axios from "axios";
import ListFilms from "../ListFilms/ListFilms";
import Loader from "../Loader/Loader";
import {connect, useDispatch, useSelector} from "react-redux";

const Films = () => {

    const films = useSelector(state => state.films.data)
    const isLoading = useSelector(state => state.films.isLoading)
    const dispatch = useDispatch()


    useEffect(()=> {
            axios('https://ajax.test-danit.com/api/swapi/films')
                .then(res => {
                    dispatch({type: 'SET_FILMS', payload: res.data})
                })
    },[])

    return (
        <div className="App">
            {isLoading && <Loader/>}
            {!isLoading && <ListFilms films={films}/>}
        </div>
    );
};

const mapStateToProps = (state) => {
    return {
        films: state.films.data,
        isLoading: state.films.isLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setFilms: (films) => dispatch({type: 'SET_FILMS', payload: films})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Films);