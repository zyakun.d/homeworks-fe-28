import React, {useEffect, useState} from 'react';
import axios from "axios";
import "./Characters.css"
import Loader from "../Loader/Loader";
import {connect, useDispatch, useSelector} from "react-redux";

const Characters = ({charactersLinks}) => {

    const characters = useSelector(state => state.characters.data)
    const isLoading = useSelector(state => state.characters.isLoading)
    const dispatch = useDispatch()

    useEffect(() => {
            const requests = charactersLinks.map(char => axios(char))
            Promise.all(requests)
                .then(res => {
                    const names = res.map(resp => resp.data.name)
                    dispatch({type: 'SET_CHARACTERS', payload: names})
                })
    },[])

        return (
            <div>
                {isLoading && <Loader/>}
                {!isLoading && characters.join(", ")}
            </div>
        );
}

const mapStateToProps = (state) => {
    return {
        characters: state.characters.data,
        isLoading: state.characters.isLoading
    }
}

const mapDispatchToProps = (dispatch) => {
    return {
        setCharacters: (characters) => dispatch({type: 'SET_CHARACTERS', payload: characters})
    }
}


export default connect(mapStateToProps, mapDispatchToProps)(Characters);