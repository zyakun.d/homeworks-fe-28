import React, {Component} from 'react';

class Button extends Component {
  render() {
    const {content, handleClick, isDisabled} = this.props;

    return <button onClick={handleClick} disabled={isDisabled}>{content}</button>;
  }
}

export default Button;