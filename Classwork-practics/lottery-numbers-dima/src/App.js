import './App.css';
import Button from "./../src/components/Button/Button";
import React, {Component} from 'react';
import Numbers from "./../src/components/Numbers/Numbers";

class App extends Component {

    state = {
        numbers: [],
        isDisable: false
    }

    generateNumber = () => {
        // return Math.random() * (max - min) + min;
        if(this.state.numbers.length >= 6){
            return
        }
        const generateNumber = Math.floor(Math.random() * 6) + 1;

        if(!this.state.numbers.includes(generateNumber)){
            this.setState({
                numbers: [...this.state.numbers, generateNumber]
            })
        }else{
            this.generateNumber()
        }
    }

    deleteNumber = (index) => {
        this.setState({
            numbers: this.state.numbers.filter((el, i) => i !== index)
        });

    }

    render() {

        const {numbers} = this.state;

        return (
            <div className="App">
                <Button
                    content={'Generate'}
                    handleClick={this.generateNumber}
                    isDisabled={this.state.numbers.length === 6}
                />
                <Numbers
                    numbers={numbers}
                    deleteNumber={this.deleteNumber}
                />

            </div>
        );
    }
}

export default App;
