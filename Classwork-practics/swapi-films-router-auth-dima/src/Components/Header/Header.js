import React from 'react';
import "./Header.css"

const Header = ({isLoggedIn, login, LogOut}) => {
    return (
        <div className="header-wrapper">
            <h4>Список всех фильмов из серии Звездные войны</h4>
            <div style={{color: "blue"}}>
                {isLoggedIn ? login : "unauthorized user"}
            </div>
            <div>
                {isLoggedIn && <button onClick={LogOut}>Log OUT</button>}
            </div>
        </div>
    );
};

export default Header;