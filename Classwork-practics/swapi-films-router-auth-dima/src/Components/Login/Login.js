import React, {useRef} from 'react';
import "./Login.css"


const Login = ({setUser}) => {

    const loginRef = useRef()
    const passwordRef = useRef()

    const handleSubmit = (e) => {
        e.preventDefault()
        const login = loginRef.current.value;
        const password = passwordRef.current.value;
        const user = {login, password};
        localStorage.setItem("authorized user", JSON.stringify(user))
        setUser(user);
    }

    return (
        <div className="login-form">
            <form onSubmit={handleSubmit}>
                <div>
                    <input type="text" placeholder="login" ref={loginRef} required/>
                </div>
                <div>
                    <input type="password" placeholder="password" ref={passwordRef} required/>
                </div>
                <div>
                    <button type="submit" >Submit</button>
                </div>
            </form>
        </div>
    );
};

export default Login;