import React from 'react';
import {Redirect, Route} from "react-router-dom";

const ProtectedRoute = ({isLoggedIn, children, ...rest}) => {
    return (
        <div>
            <Route {...rest}>
                {isLoggedIn ? children : <Redirect to="/login"/>}
            </Route>
        </div>
    );
};

export default ProtectedRoute;