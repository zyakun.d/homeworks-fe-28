import React from 'react';
import "./Film.css"
import {useHistory} from "react-router-dom";

const Film = ({film}) => {

    const history = useHistory()

        return (
            <li className="Film">
                <div className="filmsItem">
                    {film.name}
                    {<button onClick={()=>history.push(`/films/${film.id}`)}> Детальнее...</button>}
                </div>
            </li>
        );
}

export default Film;

