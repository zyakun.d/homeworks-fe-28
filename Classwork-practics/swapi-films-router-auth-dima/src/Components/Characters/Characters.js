import React, {useEffect, useState} from 'react';
import axios from "axios";
import "./Characters.css"
import Loader from "../Loader/Loader";

const Characters = ({charactersLinks}) => {

    const [isLoading, setIsLoading] = useState(true);
    const [characters, setCharacters] = useState([])

    useEffect(() => {
            const requests = charactersLinks.map(char => axios(char))
            Promise.all(requests)
                .then(res => {
                    const names = res.map(resp => resp.data.name)
                    setCharacters(names)
                    setIsLoading(false)
                })
    },[charactersLinks])

        return (
            <div>
                {isLoading && <Loader/>}
                {!isLoading && characters.join(", ")}
            </div>
        );
}

export default Characters;