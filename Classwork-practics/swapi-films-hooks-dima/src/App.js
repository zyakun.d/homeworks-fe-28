import React, {useEffect, useState} from "react";
import './App.css';
import axios from "axios";
import ListFilms from "./Components/ListFilms/ListFilms";
import Loader from "./Components/Loader/Loader";


const App = () => {

    const [isLoading, setIsLoading] = useState(true);
    const [films, setFilms] = useState([])

    useEffect(()=> {
        setTimeout(() => {
            axios('https://ajax.test-danit.com/api/swapi/films')
                .then(res => {
                    setIsLoading(false)
                    setFilms(res.data)
                })
        }, 2500)
    })

    return (
        <div className="App">
            {!isLoading && <ListFilms films={films}/>}
            {isLoading && <Loader/>}
        </div>
    );
}

export default App;
