import React, {useEffect, useState} from 'react';
import axios from "axios";
import "./Characters.css"
import Loader from "../Loader/Loader";

const Characters = ({charactersLinks}) => {

    const [isLoading, setIsLoading] = useState(true);
    const [characters, setCharacters] = useState([])

    useEffect(() => {
        setTimeout(() => {
            const requests = charactersLinks.map(char => axios(char))
            Promise.all(requests)
                .then(res => {
                    const names = res.map(resp => resp.data.name)
                    setIsLoading(false)
                    setCharacters(names)
                })

        }, 2500)
    })

        const charactersList = characters.map((char,index) => {
            return <div className="character" key={index}>
                <div>{index}</div>
                <div className="name">{char}</div>
            </div>
        })

        return (
            <div>
                {isLoading && <Loader/>}
                {!isLoading && charactersList}
            </div>
        );
}

export default Characters;