import React, {useState} from 'react';
import "./Film.css"
import Characters from "../Characters/Characters";

const Film = ({film}) => {

    const [filmIsOpen, setFilmIsOpen] = useState()

    function showMore () {
        setFilmIsOpen(true)
    }

    function showMoreOff () {
        setFilmIsOpen(false)
    }

        return (
            <li className="oneFilm">
                <div>
                    {film.name}
                    {!filmIsOpen && <button onClick={showMore}> Детальнее...</button>}
                    {filmIsOpen && <button onClick={showMoreOff}> Скрыть...</button>}
                </div>
                <div>
                    {filmIsOpen && <div>
                        {`"Эпизод": ${film.episodeId}`}
                    </div>}
                </div>
                <div>
                    {filmIsOpen && <div>
                        {`"Титры": ${film.openingCrawl}`}
                    </div>}
                </div>
                <div>
                    {filmIsOpen &&
                    <Characters charactersLinks={film.characters}/>}
                </div>
            </li>
        );
}

export default Film;

