import React from 'react';

const Loader = () => {
    return (
        <div style={{color: "red"}}>
            Data is Loading... Please wait...
        </div>
    );
};

export default Loader;