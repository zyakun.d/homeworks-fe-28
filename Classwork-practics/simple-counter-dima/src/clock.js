import React from "react";
//import {render} from "react-dom";

class Clock extends React.Component {
    constructor(props) {
        super(props);
        this.state = {date: new Date()};
    }

    componentDidMount() {
        this.timerID = setInterval(
            () => this.tick(),
            1000
        )
    }

    componentWillUnmount() {
        clearInterval(this.timerID);
    }

    tick() {
        this.setState({
            date: new Date()
        });
    }


    render() {
        return (
            <div>
                <h1>Привет всем</h1>
                <h2>Сейчас: {this.state.date.toLocaleTimeString()}.</h2>
            </div>
        )
    }
}

export default Clock

// function Clock (props) {
//     return (
// //         <div>
// //             <h1>Привет всем</h1>
// //             <h2>Сейчас:  {props.date.toLocaleTimeString()}.</h2>
//         </div>
//     )
// }
//
// function tick() {
//     render (
//         <Clock date={new Date()}/>,
//         document.getElementById('root')
//     )
// }
// setInterval(tick, 1000)
