import React from "react";
import './App.css';

class SimpleCounter extends React.Component {

    state = {
        percent: 50,
        step: 5
    }

    decreaseCounter = () => {
        const {percent, step} = this.state
        this.setState({percent: Math.max(0, percent - step)})
    }

    increaseCounter = () => {
        const {percent, step} = this.state
        this.setState({percent: Math.min(100, percent + step)})
    }

    decreaseStep = () => {
        const {step} = this.state

        if(step < 10) {
            this.setState({
                step: step + 1
            })
        }
    }

    increaseStep = () => {
        const {step} = this.state

        if(step > 1) {
            this.setState({
                step: step - 1
            })
        }
    }


    render() {
        const {percent, step} = this.state

        return (
            <div className="App">
                <div className="count">
                    <button onClick={this.increaseCounter}>+</button>
                    <div>{percent}%</div>
                    <button onClick={this.decreaseCounter}>-</button>
                </div>

                <div className="count">
                    <button onClick={this.increaseStep}>+</button>
                    <div>{step}</div>
                    <button onClick={this.decreaseStep}>-</button>
                </div>
            </div>
        );
    }
}
export default SimpleCounter;
