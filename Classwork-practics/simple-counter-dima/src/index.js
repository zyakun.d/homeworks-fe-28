import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import 'bootstrap/dist/css/bootstrap.css'

// import App from './App';
// import SimpleCounter from "./simple-counter";
import reportWebVitals from './reportWebVitals';
// import Clock from "./clock";
import TemperatureCalculator from "./temperature-calculator";

ReactDOM.render(
  <React.StrictMode>
    {/*<App />*/}
    {/*  <Clock date={new Date()}/>*/}
    {/*  <Clock/>*/}
      <TemperatureCalculator/>
  </React.StrictMode>,
  document.getElementById('root')
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
