import React, {useEffect, useState} from 'react';
import axios from "axios";
import ListFilms from "../ListFilms/ListFilms";
import Loader from "../Loader/Loader";

const Films = () => {

    const [isLoading, setIsLoading] = useState(true);
    const [films, setFilms] = useState([])

    useEffect(()=> {
            axios('https://ajax.test-danit.com/api/swapi/films')
                .then(res => {
                    setIsLoading(false)
                    setFilms(res.data)
                })
    },[])

    return (
        <div className="App">
            {isLoading && <Loader/>}
            {!isLoading && <ListFilms films={films}/>}
        </div>
    );
};

export default Films;