import React, {useEffect, useState} from 'react';
import "./OneFilms.css"
import axios from "axios";
import {useHistory, useParams} from "react-router-dom";
import Loader from "../Loader/Loader";
import Characters from "../Characters/Characters";

const OneFilms = () => {

    const [isLoading, setIsLoading] = useState(false);
    const [film, setFilm] = useState(null)

    const params = useParams()
    // const filmID = Number(params.filmID)
    // console.log("params.filmID", typeof(params.filmID))
    const history = useHistory()

    useEffect(()=> {
        setTimeout(() => {
            axios(`https://ajax.test-danit.com/api/swapi/films/${params.filmID}`)
                .then(res => {
                    setFilm(res.data)
                    setIsLoading(true)

                })
        }, 2500)
    },[])

    return (
        <div className="oneFilm">
            {!isLoading && <Loader/>}
            {isLoading &&  <div className="Film">
                    <div className="filmTitle">
                        <h2>{film.name}</h2>
                        {<button onClick={()=>history.push("/films")}>На главную</button>}
                    </div>
                    <h3>
                        {`"Эпизод": ${film.episodeId}`}
                    </h3>
                    <h4>
                        {`"Титры": ${film.openingCrawl}`}
                    </h4>
                    <h5>
                        <Characters charactersLinks={film.characters}/>
                    </h5>
                </div>
            }
        </div>
    );
};

export default OneFilms;