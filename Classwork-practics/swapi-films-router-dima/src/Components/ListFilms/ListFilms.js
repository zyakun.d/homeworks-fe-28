import React from 'react';
import Film from "../Film/Film";
import "./ListFilms.css"

const ListFilms = ({films}) => {

        const listFilms = films.map(elem => {return <Film key={elem.id} film={elem}/>})

        return (
            <ol className="listOfFilms">
                {listFilms}
            </ol>
        );
}

export default ListFilms;