import React from 'react';
import {Redirect, Route, Switch} from "react-router-dom";
import OneFilms from "../OneFilms/OneFilms";
import Error404 from "../Error404/Error404";
import Films from "../Films/Films";

const AppRouters = () => {
    return (
        <Switch>
            <Redirect exact from="/" to="/films"/>
            <Route exact path={"/films"}>
                <Films/>
            </Route>
            <Route exact path={"/films/:filmID"}>
                <OneFilms/>
            </Route>
            <Route exact path={"*"}>
                <Error404/>
            </Route>
        </Switch>
    );
};

export default AppRouters;