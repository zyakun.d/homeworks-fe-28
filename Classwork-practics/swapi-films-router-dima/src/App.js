import React from "react";
import './App.css';
import AppRouters from "./Components/AppRouters/AppRouters";


const App = () => {
    return (
        <AppRouters/>
    );
}

export default App;
