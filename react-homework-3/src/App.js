import React, {useEffect, useState} from "react";
import './App.css';
import axios from "axios";
import Header from "./components/Header/Header";
import Loader from "./components/Loader/Loader";
import AppRouters from "./components/AppRouters/AppRouters";
import Sidebar from "./components/Sidebar/Sidebar";

const title = 'Самый лучший магазин ведёрок';

const App = () => {

    const [isLoading, setIsLoading] = useState(false)
    const [isModal, setIsModal] = useState(false)
    const [buckets, setBuckets] = useState([])
    const [favorites, setFavorites] = useState(localStorage.getItem("favorites") ? JSON.parse(localStorage.getItem("favorites")) : [])
    const [cart, setCart] = useState(localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : [])

    function favoritesToggle(id) {
        const toggle = favorites.includes(id)

        if (!toggle) {
            const arr = ([...favorites, id]);
            setFavorites(arr.filter((item, index) => arr.indexOf(item) === index))

            localStorage.setItem('favorites', JSON.stringify(arr.filter((item, index) => arr.indexOf(item) === index)))
        } else {
            const arr = [...favorites];
            setFavorites(arr.filter((item) => item !== id))

            localStorage.setItem('favorites', JSON.stringify(arr.filter((item) => item !== id)))
        }
    }

    function addToCart(id){
        const arrCart = cart

        const prodInCart = arrCart.find(item => item.id === id)

        if(!prodInCart){
            const arrCartUpdate = [...arrCart, {id: id, count: 1}]

            setCart(arrCartUpdate)
            localStorage.setItem('cart', JSON.stringify(arrCartUpdate))
        }else {
            const arrCartUpdate = arrCart.map(elem => elem.id === id
                ? {...elem, count: elem.count + 1}
                : elem)

            setCart(arrCartUpdate)
            localStorage.setItem('cart', JSON.stringify(arrCartUpdate))
        }
        setIsModal(false)
    }

    function deleteFromCart(id){
        const arrCart = cart;
        const arrCartUpdate = arrCart.filter(item => item.id !== id)
        setCart(arrCartUpdate)
        localStorage.setItem('cart', JSON.stringify(arrCartUpdate))
        setIsModal(false)
    }

    function checkout(){
        setCart([])
        localStorage.setItem('cart', JSON.stringify([]))
        setIsModal(false)
    }

    function toggleModal(action){

        if(typeof(action) === "number" || action === "openModal") {
            setIsModal(action)
        }else{
            setIsModal(false)
        }
    }

    useEffect(() => {
        setTimeout(() => {
            axios("/products.json")
                .then(res => {
                    setBuckets(res.data)
                    setIsLoading(true)
                })
        }, 500)
    }, [])

        return (
            <div className="main">
                {!isLoading && <Loader/>}
                {isLoading &&
                    <div>
                        <Header title={title}/>
                        <Sidebar/>
                        <AppRouters cards={buckets}
                               favoritesArr={favorites}
                               addToCart={addToCart}
                               deleteFromCart={deleteFromCart}
                               checkout={checkout}
                               setFavorites={favoritesToggle}
                               isModal={isModal}
                               toggleModal={toggleModal}
                               cart={cart}
                        />
                    </div>}
            </div>
        );

}

export default App