import React from 'react';
import PropTypes from 'prop-types';
import "./FavoritesPage.css"
import Card from "../Card/Card";
import Modal from "../Modal/Modal";

const FavoritesPage = ({cards, setFavorites, favoritesArr, addToCart, isModal, toggleModal}) => {

    const cardsListsFavorites = cards.map(card => <div key={card.article}>
        <Card
            card={card}
            isFavorites={favoritesArr.includes(card.id)}
            setFavorites={setFavorites}
            isModal={isModal}
            toggleModal={toggleModal}
        />
    </div>)

    return (
        <div>
            <div className="cardList">
                {cardsListsFavorites}
            </div>
            {isModal && <Modal
                id={isModal}
                functionOkBtn={addToCart}
                toggleModal={toggleModal}
                colorBG={"orange"}
                textHeader={'Добавление товара в корзину'}
                textMain={'Вы действительно хотите добавить выбранный товар в корзину ?'}
            />}
        </div>
    );
};

FavoritesPage.prototype = {
    cards: PropTypes.array,
    setFavorites: PropTypes.func,
    favoritesArr: PropTypes.array,
    addToCart: PropTypes.func,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number,
        PropTypes.string
    ]),
    toggleModal: PropTypes.func
}

export default FavoritesPage;