import React from "react";
import "./Modal.scss"
import Button from "../Button/Button";
import PropTypes from 'prop-types';

const Modal = ({id, functionOkBtn, toggleModal, colorBG, textHeader, textMain}) => {

        return (
            <div>
                <div className="modalOverlay" onClick={() => {toggleModal("Close")}}/>
                <div className="modalWindow"
                     style={{
                         backgroundColor: colorBG,
                     }}
                >
                    <div className="modalHeader"
                         style={{
                             backgroundColor: colorBG,
                         }}
                    >
                        <>{textHeader}</>
                        <>{<div className="modalCrossWrapper" onClick={() => {toggleModal("Close")}}>
                            <svg
                                className="modalCross"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24">
                                <path fill="white"
                                      d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/>
                            </svg>
                        </div>
                        }</>
                    </div>
                    <div className="modalText">
                        {textMain}
                    </div>
                    <div className="modalFooter">
                        <Button
                            text="Ок"
                            id={id}
                            onClick={functionOkBtn}
                        />
                        <Button
                            text="Отмена"
                            onClick={() => {toggleModal("Close")}}
                        />
                    </div>
                </div>
            </div>
        )
}

Modal.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    functionOkBtn: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired,
    colorBG: PropTypes.string,
    textHeader: PropTypes.string,
    textMain: PropTypes.string
};

export default Modal