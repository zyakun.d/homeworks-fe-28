import React from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import Favorite from "../Favorite/Favorite";
import "./Card.css"
import DeleteCross from "../DeleteCross/DeleteCross";

const Card = ({card, setFavorites, isFavorites, isModal, toggleModal, flag, quantity}) => {

        return (
            <div className={flag==='cart' ? "cardCart" : "card"}>
                {!flag && <div className="favoriteStar" onClick={() => setFavorites(card.id)}>
                    <Favorite
                        filled={(!!isFavorites)}
                        color={"red"}/>
                </div>}
                <div className={flag==='cart' ? "cardCartImg" : "cardImg"}>
                    <img src={card.src} alt="#"/>
                </div>
                <div className={flag==='cart' ? "cardCartTitle" : "cardTitle"}>
                    {card.name}
                </div>
                <div className="cardArticle">
                    <span className={flag==='cart' ? "spanCartArticle" : "spanArticle"}>
                        {flag==='cart' ? '' : "Article:"}
                    </span>{card.article}
                </div>
                <div className={flag==='cart' ? "cardCartPrice" : "cardPrice"}>
                    {card.price}<span className="spanPrice">
                    {!flag==='cart' ? '' : " UAH"}
                    </span>
                </div>
                {!flag && <div className="purchaseBtn">
                    {!isModal && <Button
                        text="Купить"
                        onClick={() => {
                            toggleModal(card.id)
                        }}
                    />}
                </div>}
                {flag && <div className="quantityCart">
                    {quantity}<span className="spanQuantity">
                    {!flag==='cart' ? '' : " шт"}
                    </span>
                </div>}
                {flag && <div className="amountTotal">
                    {(quantity)*card.price}<span className="spanAmount">
                    {!flag==='cart' ? '' : " UAH"}
                    </span>
                </div>}
                {flag && <div className="deleteCors"
                              onClick={() => {
                                  toggleModal(card.id)
                              }}>
                    <DeleteCross color={"red"}
                    />
                </div>}

            </div>
        );
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
    setFavorites: PropTypes.func,
    isFavorites: PropTypes.bool,
    toggleModal: PropTypes.func.isRequired,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number,
        PropTypes.string
    ]),
    flag: PropTypes.string,
    quantity: PropTypes.number
};

export default Card;