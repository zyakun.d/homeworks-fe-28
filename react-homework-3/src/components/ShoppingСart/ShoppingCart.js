import React, {useEffect, useState} from 'react';
import PropTypes from 'prop-types';
import "./ShoppingCart.css"
import axios from "axios";
import Card from "../Card/Card";
import Modal from "../Modal/Modal";
import Button from "../Button/Button";

const ShoppingCart = ({cards, cart, isModal, toggleModal, deleteFromCart, checkout}) => {

    const [cardsInCart, setCardsInCart] = useState(cards)

    useEffect(() => {
        axios("/products.json")
            .then(res => {
                const arr = res.data;
                const array = arr.filter(card => cart.some(elem => card.id === elem.id))
                setCardsInCart(array)
            })
    }, [cart])

    const cardsListsCart = cart.map(item => <div key={item.id}>
        <Card card={cardsInCart.find(card => card.id === item.id)}
              isModal={isModal}
              toggleModal={toggleModal}
              flag={'cart'}
              quantity={item.count}
        />
    </div>)

    let totalCountAmount = 0
    cart.forEach((item) => {
        const price = cardsInCart.find(card => card.id === item.id).price
        totalCountAmount = totalCountAmount + (price * item.count)
    })

    return (
        <div>
            <div className="cart-header">
                {cart.length === 0 ? <h3>Товаров в корзине нет</h3> : <h3>Товары в корзине</h3>}
            </div>
            <div className="cart-main">
                {cardsListsCart}
                {typeof(isModal) === "number" && <Modal
                    id={isModal}
                    functionOkBtn={deleteFromCart}
                    toggleModal={toggleModal}
                    colorBG={"red"}
                    textHeader={'Удаление товара из корзины'}
                    textMain={'Вы действительно хотите удалить выбранный товар из корзины ?'}
                />}
            </div>
            <div className="cart-footer">
                {cart.length !== 0 && <h3>Общая сумма заказа составляет: {totalCountAmount} UAH</h3>}
                {cart.length !== 0 && <Button text={'Оформить покупку'}
                         onClick={() => {
                             toggleModal("openModal")
                         }}
                />}
                {isModal === "openModal" && <Modal
                    id={isModal}
                    functionOkBtn={checkout}
                    toggleModal={toggleModal}
                    colorBG={"green"}
                    textHeader={'Оформление покупки'}
                    textMain={'Вы действительно желаете отправить товары из корзины на точку выдачи?'}
                />}
            </div>
        </div>
    );
};

ShoppingCart.propTypes = {
    cards: PropTypes.array,
    cart: PropTypes.array,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number,
        PropTypes.string
    ]),
    toggleModal: PropTypes.func,
    deleteFromCart: PropTypes.func,
    checkout: PropTypes.func
}

export default ShoppingCart;