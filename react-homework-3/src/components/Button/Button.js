import React from "react";
import PropTypes from 'prop-types';
import './Button.scss';

const Button = ({id, onClick, text}) => {
    return (
        <div>
            <button
                className={`buttonAddToCart`}
                onClick={id ? () => onClick(id) : () => onClick()}
            >
                {text}
            </button>
        </div>
    );
};

Button.propTypes = {
    id: PropTypes.oneOfType([
        PropTypes.number,
        PropTypes.string
    ]),
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};

export default Button;