import React from 'react';
import PropTypes from 'prop-types';
import {Redirect, Route, Switch} from "react-router-dom";
import Cards from "../Cards/Cards";
import FavoritesPage from "../FavoritesPage/FavoritesPage";
import ShoppingCart from "../ShoppingСart/ShoppingCart";
import Error404 from "../Error404/Error404";

const AppRouters = ({cards, addToCart, favoritesArr, setFavorites, isModal, toggleModal, cart, deleteFromCart, checkout}) => {

    return (
        <Switch>
            <Redirect exact from={"/"} to={"/shop"}/>
            <Route exact path="/shop">
                <Cards cards={cards}
                       favoritesArr={favoritesArr}
                       addToCart={addToCart}
                       setFavorites={setFavorites}
                       isModal={isModal}
                       toggleModal={toggleModal}
                />
            </Route>
            <Route exact path="/favorites">
                <FavoritesPage cards={cards.filter(card=>favoritesArr.some(id => card.id === id))}
                               favoritesArr={favoritesArr}
                               addToCart={addToCart}
                               setFavorites={setFavorites}
                               isModal={isModal}
                               toggleModal={toggleModal}
                />
            </Route>
            <Route exact path="/cart">
                <ShoppingCart cards={cards.filter(card=>cart.some(elem => card.id === elem.id))}
                              cart={cart}
                              isModal={isModal}
                              toggleModal={toggleModal}
                              deleteFromCart={deleteFromCart}
                              checkout={checkout}
                />
            </Route>
            <Route exact path="*">
                <Error404/>
            </Route>
        </Switch>
    );
};

AppRouters.prototype = {
    cards: PropTypes.array,
    addToCart: PropTypes.func,
    favoritesArr: PropTypes.array,
    setFavorites: PropTypes.func,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number,
        PropTypes.string
    ]),
    toggleModal: PropTypes.func,
    cart: PropTypes.array,
    deleteFromCart: PropTypes.func,
    checkout: PropTypes.func
}

export default AppRouters;