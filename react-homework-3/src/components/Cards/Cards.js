import React from 'react';
import PropTypes from 'prop-types';
import Card from "../Card/Card";
import "./Cards.css"
import Modal from "../Modal/Modal";

const Cards = ({cards, setFavorites, favoritesArr, addToCart, isModal, toggleModal}) => {

    const cardsLists = cards.map(card => <div key={card.article}>
        <Card
            card={card}
            isFavorites={favoritesArr.includes(card.id)}
            setFavorites={setFavorites}
            isModal={isModal}
            toggleModal={toggleModal}
        />
    </div>)

    return (
        <div>
            <div className="cardList">
                {cardsLists}
            </div>
            {isModal && <Modal
                id={isModal}
                functionOkBtn={addToCart}
                toggleModal={toggleModal}
                colorBG={"orange"}
                textHeader={'Добавление товара в корзину'}
                textMain={'Вы действительно хотите добавить выбранный товар в корзину ?'}
            />}
        </div>
    );
};

Cards.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.object),
    setFavorites: PropTypes.func,
    favoritesArr: PropTypes.array,
    addToCart: PropTypes.func.isRequired,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number,
        PropTypes.string
    ]),
    toggleModal: PropTypes.func
};

export default Cards;