import React, {Component} from "react";
import './App.css';
import Button from "./components/Button/Button";
import Modal from "./components/Modal/Modal";

const modalOne =
    {
        colorPrimary: '#d34537',
        colorSecondary: '#e74b3b',
        colorBtn: '#b3382b',
        header: 'Подтверждение заказа',
        text: 'Для подтверждения Вашего заказа нажмите кнопочку "Ок" или отмените Ваш заказ',
        crossButton: true,
    }

const modalTwo =
    {
        colorPrimary: '#040c84',
        colorSecondary: '#3a4099',
        colorBtn: '#060b54',
        header: 'Поздравление',
        text: 'Поздравляем, Ваш заказ принят. Продолжим покупки ?',
        crossButton: false,
    }

const btnOne =
    {
        id: 1,
        color: 'red',
        name: 'модалка №1'
    }

const btnTwo =
    {
        id: 2,
        color: 'green',
        name: 'модалка №2'
    }


class App extends Component {

    state = {
        modalIsOpen: false,
    }

    modalOpen = (id) => {
        this.setState({
            modalIsOpen: `modal${id}`
        })
    }

    modalApply = () => {
        this.setState({
            modalIsOpen: false
        })
    }

    modalClose = () => {
        this.setState({
            modalIsOpen: false
        })
    }

    render() {

        return (
            <div className="App">
                {!this.state.modalIsOpen && <Button
                    id={btnOne.id}
                    backgroundColorBtn={btnOne.color}
                    textBtn={btnOne.name}
                    onClickBtn={this.modalOpen}
                />}
                {!this.state.modalIsOpen && <Button
                    id={btnTwo.id}
                    backgroundColorBtn={btnTwo.color}
                    textBtn={btnTwo.name}
                    onClickBtn={this.modalOpen}
                />}
                {this.state.modalIsOpen && <Modal
                    data={(this.state.modalIsOpen === "modal1"
                            ? modalOne
                            : modalTwo
                    )}
                    modalClose={this.modalClose}
                    actions={[
                        <Button
                            key={'left'}
                            backgroundColorBtn={(this.state.modalIsOpen === "modal1" ?
                                `${modalOne.colorBtn}` :
                                `${modalTwo.colorBtn}`)}
                            textBtn={'Ok'}
                            className={(`modalBtn` +
                                `${this.state.modalIsOpen === "modal1"
                                    ? 1
                                    : 2}
                                `)}
                            onClickBtn={this.modalApply}
                        />,
                        <Button
                            key={'right'}
                            backgroundColorBtn={(this.state.modalIsOpen === "modal1" ?
                                `${modalOne.colorBtn}` :
                                `${modalTwo.colorBtn}`)}
                            textBtn={'Cancel'}
                            className={(`modalBtn` +
                                `${this.state.modalIsOpen === "modal1"
                                    ? 1
                                    : 2}
                                    `)}
                            onClickBtn={this.modalClose}
                        />
                    ]}
                />}
            </div>
        );
    }

}

export default App