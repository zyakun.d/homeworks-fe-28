import React, {Component} from "react";
import './Button.scss';

class Button extends Component{
        render() {
                const {backgroundColorBtn, id, textBtn, onClickBtn} = this.props;

                return (
                    <button
                        style={{
                            backgroundColor: `${backgroundColorBtn}`
                        }}
                        className={backgroundColorBtn}
                        onClick={() => onClickBtn(id)}
                    >
                        {textBtn}
                    </button>
                )
        }
}

export default Button;