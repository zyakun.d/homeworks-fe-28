import React, {Component} from "react";
import "./Modal.scss"
import Button from "../Button/Button";
import PropTypes from 'prop-types';

class Modal extends Component {

    render() {
        const {id, addToCart, toggleModal} = this.props

        return (
            <div>
                <div className="modalOverlay" onClick={() => {toggleModal("Close")}}/>
                <div className="modalWindow"
                     style={{
                         backgroundColor: "red",
                     }}
                >
                    <div className="modalHeader"
                         style={{
                             backgroundColor: "red",
                         }}
                    >
                        <>{"Подтверждение покупки"}</>
                        <>{<div className="modalCrossWrapper" onClick={() => {toggleModal("Close")}}>
                            <svg
                                className="modalCross"
                                xmlns="http://www.w3.org/2000/svg"
                                viewBox="0 0 24 24">
                                <path fill="white"
                                      d="M23.954 21.03l-9.184-9.095 9.092-9.174-2.832-2.807-9.09 9.179-9.176-9.088-2.81 2.81 9.186 9.105-9.095 9.184 2.81 2.81 9.112-9.192 9.18 9.1z"/>
                            </svg>
                        </div>
                        }</>
                    </div>
                    <div className="modalText">
                        {"Подтверждаете добавление товара в корзину ?"}
                    </div>
                    <div className="modalFooter">
                        <Button
                            text="Ок"
                            id={id}
                            onClick={addToCart}
                        />
                        <Button
                            text="Отмена"
                            onClick={() => {toggleModal("Close")}}
                        />
                    </div>
                </div>
            </div>
        )
    }
}

Modal.propTypes = {
    id: PropTypes.number.isRequired,
    addToCart: PropTypes.func.isRequired,
    toggleModal: PropTypes.func.isRequired
};

export default Modal