import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Card from "../Card/Card";
import "./Cards.css"
import Modal from "../Modal/Modal";

class Cards extends Component {
    render() {

        const {cards, setFavorites, favoritesArr, addToCart, isModal, toggleModal} = this.props;

        const cardsLists = cards.map(card => <div key={card.article}>
            <Card
                card={card}
                isFavorites={favoritesArr.includes(card.id)}
                setFavorites={setFavorites}
                isModal={isModal}
                toggleModal={toggleModal}
            />
        </div>)

        return (
            <>
                <div className="cardList">
                    {cardsLists}
                </div>
                {isModal && <Modal
                    id={isModal}
                    addToCart={addToCart}
                    toggleModal={toggleModal}
                />}
            </>
        );
    }
}

Cards.propTypes = {
    cards: PropTypes.arrayOf(PropTypes.object),
    setFavorites: PropTypes.func.isRequired,
    favoritesArr: PropTypes.array.isRequired,
    addToCart: PropTypes.func.isRequired,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number
    ]),
    toggleModal: PropTypes.func.isRequired
};

export default Cards;