import React, {Component} from "react";
import PropTypes from 'prop-types';
import './Button.scss';

class Button extends Component{
        render() {
                const {id, onClick, text} = this.props;

                return (
                    <button
                        className={`buttonAddToCart`}
                        onClick={() => onClick(id)}
                    >
                        {text}
                    </button>
                )
        }
}

Button.propTypes = {
    id: PropTypes.number,
    onClick: PropTypes.func.isRequired,
    text: PropTypes.string.isRequired
};

export default Button;