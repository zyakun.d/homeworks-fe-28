import React, {Component} from 'react';
import PropTypes from 'prop-types';
import Button from "../Button/Button";
import Favorite from "../Favorite/Favorite";
import "./Card.css"

class Card extends Component {

    render() {

        const {card, setFavorites, isFavorites, isModal, toggleModal} = this.props;

        return (
            <div className="card">
                <div className="favoriteStar" onClick={() => setFavorites(card.id)}>
                    <Favorite
                        filled={(!!isFavorites)}
                        color={"red"}/>
                </div>
                <div className="cardImg">
                    <img src={card.src} alt="#" width={220} height={220}/>
                </div>
                <div className="cardTitle">
                    {card.name}
                </div>
                <div className="cardArticle">
                    <span className="spanArticle">Article:</span>{card.article}
                </div>
                <div className="cardPrice">
                    {card.prices}<span className="spanPrice"> UAH</span>
                </div>
                <div className="purchaseBtn">
                    {!isModal && <Button
                        text="Купить"
                        onClick={() => {
                            toggleModal(card.id)
                        }}

                    />}
                </div>
            </div>
        );
    }
}

Card.propTypes = {
    card: PropTypes.object.isRequired,
    setFavorites: PropTypes.func.isRequired,
    isFavorites: PropTypes.bool.isRequired,
    isModal: PropTypes.oneOfType([
        PropTypes.bool,
        PropTypes.number
    ]),
    toggleModal: PropTypes.func.isRequired
};

export default Card;