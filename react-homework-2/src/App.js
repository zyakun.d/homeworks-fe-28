import React, {Component} from "react";
import './App.css';
import axios from "axios";
import Cards from "./components/Cards/Cards";
import Header from "./components/Header/Header";

const title = 'Самый лучший магазин ведёрок';

class App extends Component {

    state = {
        isLoading: false,
        isModal: false,
        buckets: [],
        favorites: localStorage.getItem("favorites") ? JSON.parse(localStorage.getItem("favorites")) : [],
        cart: localStorage.getItem("cart") ? JSON.parse(localStorage.getItem("cart")) : []
    }

    setFavorites = (id) => {
        const toggle = this.state.favorites.includes(id)
        if (!toggle) {
            const arr = ([...this.state.favorites, id]);
            this.setState({
                favorites: arr.filter((item, index) => arr.indexOf(item) === index)
            });
            localStorage.setItem('favorites', JSON.stringify(arr.filter((item, index) => arr.indexOf(item) === index)))
        } else {
            const arr = [...this.state.favorites];
            this.setState({
                favorites: arr.filter((item) => item !== id)
            });
            localStorage.setItem('favorites', JSON.stringify(arr.filter((item) => item !== id)))
        }
    }

    addToCart = (id) => {
        const arrCart = this.state.cart

        const prodInCart = arrCart.find(item => item.id === id)

        if(!prodInCart){
            const arrCartUpdate = [...arrCart, {id: id, count: 1}]
            this.setState({
                cart: arrCartUpdate
            });
            localStorage.setItem('cart', JSON.stringify(arrCartUpdate))
        }else {
            const arrCartUpdate = arrCart.map(elem => elem.id === id
                ? {...elem, count: elem.count + 1}
                : elem)
            this.setState({
                cart: arrCartUpdate
            });
            localStorage.setItem('cart', JSON.stringify(arrCartUpdate))
        }
        this.setState({isModal: false})
    }

    toggleModal = (action) => {

        if(typeof(action) === "number") {
            this.setState({isModal: action})
        }else{
            this.setState({isModal: false})
        }

    }

    componentDidMount() {
        setTimeout(() => {
            axios("/products.json")
                .then(res => {
                    this.setState({buckets: res.data, isLoading: true})
                })
        }, 0)
    }

    render() {

        const {buckets} = this.state;

        return (
            <div className="main">
                <Header title={title}/>
                <Cards cards={buckets}
                       favoritesArr={this.state.favorites}
                       addToCart={this.addToCart}
                       setFavorites={this.setFavorites}
                       isModal={this.state.isModal}
                       toggleModal={this.toggleModal}
                />
            </div>
        );
    }

}

export default App