import React, {useEffect} from "react";
import './App.css';
import axios from "axios";
import Header from "./components/Header/Header";
import Loader from "./components/Loader/Loader";
import AppRouters from "./components/AppRouters/AppRouters";
import Sidebar from "./components/Sidebar/Sidebar";
import {useDispatch, useSelector} from "react-redux";
import {bucketsOperations, bucketsSelectors} from "./store/shop";

const title = 'Самый лучший магазин ведёрок';

const App = () => {

    const dispatch = useDispatch()
    const buckets = useSelector(bucketsSelectors.getBuckets())
    const isLoading = useSelector(bucketsSelectors.isLoading())

    useEffect(() => {
        setTimeout(() => {
            axios("/api/shop")
                .then(res => {
                   dispatch(bucketsOperations.setBuckets(res.data))
                    console.log('Первая загрузка')
                })
        }, 500)
    }, [dispatch])

    return (
        <div className="main">
            {!isLoading && <Loader/>}
            {isLoading &&
            <div>
                <Header title={title}/>
                <Sidebar/>
                <AppRouters
                    cards={buckets}
                />
            </div>}
        </div>
    );

}

export default App