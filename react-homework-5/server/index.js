const express = require('express')

const PORT = 8000

const app = express()

const buckets = [
    {"id": 1, "name": "Plastic circle bucket 10L Black", "src": "/Img/bucket_black.jpg", "article": "pl-10-c-blk", "price": 150},
    {"id": 2, "name": "Plastic circle bucket 15L Blue", "src": "/Img/bucket_blue.jpg", "article": "pl-15-c-bl", "price": 200},
    {"id": 3, "name": "Plastic circle bucket 10L Green", "src": "/img/bucket_green.jpg", "article": "pl-10-c-grn", "price": 160},
    {"id": 4, "name": "Plastic square bucket 8L Grey", "src": "/img/bucket_grey.jpg", "article": "pl-8-sq-gr", "price": 180},
    {"id": 5, "name": "Plastic circle bucket 20L Olive", "src": "/img/bucket_olive.jpg", "article": "pl-20-c-olv", "price": 300},
    {"id": 6, "name": "Plastic circle bucket 6L Orange", "src": "/img/bucket_orange.jpg", "article": "pl-06-c-or", "price": 100},
    {"id": 7, "name": "Plastic circle bucket 8L pink", "src": "/img/bucket_pink.jpg", "article": "pl-08-c-pnk", "price": 110},
    {"id": 8, "name": "Plastic circle bucket 15L Purple", "src": "/img/bucket_purple.jpg", "article": "pl-15-c-prp", "price": 200},
    {"id": 9, "name": "Plastic oval bucket 20L Red", "src": "/img/bucket_red.jpg", "article": "pl-20-ovl-rd", "price": 350},
    {"id": 10, "name": "Metallic oval bucket 7L Silver", "src": "/img/bucket_silver.jpg", "article": "mt-07-c-slv", "price": 400},
    {"id": 11, "name": "Plastic circle bucket 12L White", "src": "/img/bucket_white.jpg", "article": "pl-12-c-w", "price": 170},
    {"id": 12, "name": "Plastic circle bucket 15L Yellow", "src": "/img/bucket_yellow.jpg", "article": "pl-15-c-ylw", "price": 190}
]


app.get('/api/shop', (req, res) => {
    res.send(buckets)
})

app.listen(PORT, () => {
    console.log(`Server listening on port ${PORT}`)
})